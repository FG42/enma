;;; Enma --- A functional build tool and package manager for GNU/Emacs
;;
;; Copyright (c) 2020  Sameer Rahmani <lxsameer@gnu.org>
;;
;; Author: Sameer Rahmani <lxsameer@gnu.org>
;; URL: https://gitlab.com/FG42/enma
;; Version: 0.1.0
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;
;;; Commentary:
;;; Code:
(require 'cl-lib)
(require 'enma/process)


(defun enma-backends-git/fetch-source (repo commit path)
  "Fetch the given REPO at the given COMMIT and store it in PATH."
  (message
   (shell-command-to-string
    (format "git clone --depth 1 -b %s %s %s" commit repo path))))


(defun enma-backends-git/sentinel (process event))


(cl-defun enma-backends-git/clone
    (repo version target &key (depth 1)
          (sentinel-fn #'enma-backends-git/sentinel))
  "Clone the given REPO at given VERSION in the given TARGET.
DEPTH is the value of --depth parameter of git-clone.
SENTINEL-FN is the function that act as the event handler of
the git process."
  (let ((buff (get-buffer-create "*git-clone*")))
    (enma-process/run
     (list "git"
           "clone"
           "--depth" (number-to-string depth)
           "--no-checkout"
           "--branch" version
           repo target)
     :buffer buff)))


(defun enma-backends-git/fetch-package (state pkg)
  "Fetch the given PKG via git."
  (let ((dest ))))


(provide 'enma/backends/git)
;;; git.el ends here

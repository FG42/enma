;;; Enma --- A functional build tool and package manager for GNU/Emacs
;;
;; Copyright (c) 2020  Sameer Rahmani <lxsameer@gnu.org>
;;
;; Author: Sameer Rahmani <lxsameer@gnu.org>
;; URL: https://gitlab.com/FG42/enma
;; Version: 0.1.0
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;
;;; Commentary:
;;
;;; Code:
(defconst enma/task-prefix "enma-task/%s"
  "The default prefix for tasks.")


(defconst enma/help-prefix "enma-task-help/%s"
  "The default prefix for task's help var.")


(defvar enma/tasks-plist '()
  "A list of all the tasks registered in the build system.")


(defmacro deftask (name args docstring &rest body)
  "Define an Enma task with the given NAME, ARGS, DOCSTRING and BODY.
All it does is to creates a function by prefixing the given NAME using
`enma-task/' to mark it for later to be find by the CLI tool.
Please note that the `docstring is mandatory and will be used as the
help string on the CLI."
  (declare (indent defun) (doc-string 3))
  (let ((fn-name (format enma/task-prefix name))
        (var-name (format enma/help-prefix name))
        (first-line-of-doc (car (split-string docstring "\\\n"))))
    `(progn
       (setq enma/tasks-plist
             (plist-put enma/tasks-plist
                        ,(symbol-name name)
                        ,first-line-of-doc))
       (defvar ,(intern var-name) ,docstring)
       (defun ,(intern fn-name) ,args ,docstring ,@body))))


(provide 'enma/tasks)
;;; tasks.el ends here

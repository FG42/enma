;;; Enma --- A functional build tool and package manager for GNU/Emacs
;;
;; Copyright (c) 2020  Sameer Rahmani <lxsameer@gnu.org>
;;
;; Author: Sameer Rahmani <lxsameer@gnu.org>
;; URL: https://gitlab.com/FG42/enma
;; Version: 0.1.0
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;
;;; Commentary:
;;
;;; Code:
(require 'enma/utils)
(require 'enma/state)


(defun enma-recipe/-load-recipe (path package-name)
  "Construct the recipe file name using PATH and PACKAGE-NAME and load it."
  (read (or (read-file (path-join path
                                  (format "%s" package-name)))
            "()")))


(defun enma-recipe/lookup (package-name paths)
  "Lookup the recipe for the given PACKAGE-NAME in the given PATHS."
  (when paths
    (or (enma-recipe/-load-recipe (car paths) package-name)
        (enma-recipe/lookup package-name (cdr paths)))))


(defun enma-recipe/get (state package-name)
  "Return the recipe of the given PACKAGE-NAME for the STATE."
  (let ((paths (enma-state/recipe-paths state)))
    (enma-recipe/lookup package-name paths)))


(provide 'enma/recipies)
;;; recipies.el ends here

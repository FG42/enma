;;; Enma --- A functional build tool and package manager for GNU/Emacs
;;
;; Copyright (c) 2020  Sameer Rahmani <lxsameer@gnu.org>
;;
;; Author: Sameer Rahmani <lxsameer@gnu.org>
;; URL: https://gitlab.com/FG42/enma
;; Version: 0.1.0
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;
;;; Commentary:
;;; Code:
(require 'cl-lib)
(require 'enma/utils)
(require 'enma/backends/git)


(defconst enma-sources/source-directory "sources/")

(defconst enma-sources/default-sources
  '((melpa . (:type 'git
                    :name "melpa"
                    :recipe-path "recipes"
                    :url "https://github.com/melpa/melpa.git"
                    :version "master")))
  "A list of default sources for Enma to download recipes from.")


(defun enma-sources/source-path (project)
  "Return the path to source directory of the PROJECT.
Source directory is the place which we store different source repositories."
  (concat
   (file-name-as-directory
    (enma/project-enma-directory project))
   enma-sources/source-directory))


(defun enma-sources/-fetch-source (state source path)
  "Fetch a source repository with the given SOURCE details under PATH.
STATE is the global state of the project."
  (let* ((type (eval (plist-get source :type)))
         (url (plist-get source :url))
         (version (plist-get source :version))
         (name    (plist-get source :name))
         (recipe-path (plist-get source :recipe-path))
         (source-fn (->fn "enma-backends-%s/fetch-source" type)))

    ;; TODO: Extract the source validation to another function
    (when (or (not type) (not (symbolp type)))
      (throw 'source-error
             (format "Don't know about the source type '%s'" type)))

    (when (not name)
      (throw 'source-error
             "Source name is missing."))

    (when (not source-fn)
      (throw 'source-type-error
             (format "Don't know about the source type '%s" type)))

    ;; TODO: check existence of the given source before installing it.
    (let* ((dest (path-join path name))
           (dest-recipe (path-join (format "%s/" dest) recipe-path)))
      (funcall source-fn url version dest)

      ;; If the process crashes before setting the state, no big deal.
      ;; Because user will see an error anyway and has to retry.
      (-> state
          (enma-state/cons :sources (cons name (list source)))
          (enma-state/->recipe-paths! dest-recipe)))))




(defun enma-sources/lookup-source (source)
  "Find the details of the given SOURCE in Enma's default sources."
  (cdr (assoc source enma-sources/default-sources)))


(defun enma-sources/fetch-source (state source path)
  "Fetch content of the given the given SOURCE and store it under PATH.
STATE is the global state of the project."
  (cond
   ((symbolp source) (enma-sources/-fetch-source
                      state
                      (enma-sources/lookup-source source)
                      path))
   ((listp source) (enma-sources/-fetch-source state source path))
   (t (throw 'source-error
             (format  "Don't know how to fetch this source: %s" source)))))


(defun enma-sources/fetch-sources (state project)
  "Fetches the meta data and recipes for all the sources of the given PROJECT.
STATE is the global state of the project."
  (when (not project)
    (throw 'project-error "Invalid project."))

  (let ((source-dir (enma-sources/source-path project)))
    (when (not (file-exists-p source-dir))
      (make-directory source-dir t))
    (seq-reduce
     (lambda (acc-state source)
       (enma-sources/fetch-source acc-state source source-dir))
     (enma/project-sources project)
     state)))


(provide 'enma/sources)
;;; sources.el ends here

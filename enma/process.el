;;; Enma --- A functional build tool and package manager for GNU/Emacs
;;
;; Copyright (c) 2020  Sameer Rahmani <lxsameer@gnu.org>
;;
;; Author: Sameer Rahmani <lxsameer@gnu.org>
;; URL: https://gitlab.com/FG42/enma
;; Version: 0.1.0
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;
;;; Commentary:
;;; Code:
(require 'cl-lib)

(defun enma-process/global-sentinel (process event)
  "The global sentinel function for enma processes.
PROCESS is an Emacs `process` instance which the given EVENT is emitted
for."
  (cond
   ((string= event "stopped") (message "xxxxxxxxxxxxx"))
   (t event)))


(cl-defun enma-process/run
    (command &key buffer filter-fn
             (sentinel-fn #'enma-process/global-sentinel)
             (path default-directory))
  "Run the given COMMAND as an async process.
COMMAND is a list which contains the command itself alongside with
its parameters as the rest of the elements.

BUFFER is the buffer the will receive the stdout and stderr of the
process.

FILTER-FN is a function which will receive the stdout and stderr
of the process.  If it is nil the the default filter function
will be used that insert everything into the BUFFER.

SENTINEL-FN is the function which is in charge of event handling
of the process.

the process will be run in with PATH set as the `default-directory'."
  (let ((default-directory path))
    (make-process
     :name (car command)
     :command command
     :buffer buffer
     :filter filter-fn
     :stderr nil
     :sentinel sentinel-fn)))


(provide 'enma/process)
;;; process.el ends here

;;; Enma --- A functional build tool and package manager for GNU/Emacs
;;
;; Copyright (c) 2020  Sameer Rahmani <lxsameer@gnu.org>
;;
;; Author: Sameer Rahmani <lxsameer@gnu.org>
;; URL: https://gitlab.com/FG42/enma
;; Version: 0.1.0
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;
;;; Commentary:
;;; Glossory:
;; `enma-directory': A directory which Enma uses to store all the required
;;                   information to work. Including caches, state, packages...
;;; Code:
(require 'cl-lib)
(require 'enma/utils)
(require 'enma/sources)


(defvar enma/target-project nil
  "An instance of `enma-project' that describes the target project of Enma.")


(cl-defstruct enma-project
  name
  version
  dependencies
  ;; Where to download the recipes and packages
  (sources enma/default-sources)
  ;; The default path to store cache, packages and the state of Enma
  (enma-dir ".enma")
  (state-file "enma.state")
  (state nil))


(defmacro defproject (name version &rest body)
  "Define a project with the given NAME, VERSION and BODY.
BODY can contain the following parameters:
 `:dependencies' a list of dependenices of the current project in the form of
  (name version &optional recipe)."
  (declare (indent defun))
  `(progn
     (setq enma/target-project
           (make-enma-project
            :name ,(symbol-name name)
            :version ,version
            ,@body))
     (setf (enma-project-enma-dir enma/target-project)
           (expand-file-name (enma-project-enma-dir enma/target-project)))
     (setq default-directory
           (enma-project-enma-dir enma/target-project))))


(defun enma/project-name (project)
  "Return name of the given PROJECT."
  (enma-project-name project))


(defun enma/project-dependencies (project)
  "Return a list of dependencies for the given PROJECT."
  (enma-project-dependencies project))


(defun enma/project-sources (project)
  "Return a list of package sources for the given PROJECT."
  (enma-project-sources project))


(defun enma/project-enma-directory (project)
  "Return the enma directory associated with the given PROJECT."
  (enma-project-enma-dir project))


(defun enma/project-version (project)
  "Return version of the given PROJECT."
  (enma-project-version project))


(defun enma/project-state (project)
  "Return state attached to the given PROJECT."
  (enma-project-state project))


(defun enma/project-state-file (project)
  "Return state attached to the given PROJECT."
  (path-join
   (enma-project-enma-dir project)
   (enma-project-state-file project)))


(defun enma/project-set-state! (project state)
  "Attach the given STATE to the PROJECT."
  (setf (enma-project-state project)
        state))


(defun enma/project-version (project)
  "Return version of the given PROJECT."
  (enma-project-version project))


(defun enma/project-reset-target! (&optional value)
  "Reset the value of the given VALUE."
  (setq enma/target-project value))


(provide 'enma/projects)
;;; projects.el ends here

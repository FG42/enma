;;; Enma --- A functional build tool and package manager for GNU/Emacs -*- lexical-binding: t -*-
;;
;; Copyright (c) 2020  Sameer Rahmani <lxsameer@gnu.org>
;;
;; Author: Sameer Rahmani <lxsameer@gnu.org>
;; URL: https://gitlab.com/FG42/enma
;; Version: 0.1.0
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;
;;; Commentary:
;;
;;; Code:
(defun ->project-path (dir)
  "Return the absolute path to the given DIR with respect to FG42_HOME."
  (concat enma/target-project-path (format "/%s" dir)))


(defun find-files (dir suffix)
  "Find all the files with the given SUFFIX in the given DIR."
  (split-string (shell-command-to-string
                 (format "find %s -iname \"*.%s\"" (->project-path dir) suffix)) "\n" t))


(defun el-files-in (dir)
  "Return a list of elisp files in the given DIR."
  (find-files dir "el"))


(defun elc-files-in (dir)
  "Return a list of elisp files in the given DIR."
  (find-files dir "elc"))



;; IMPORTANT: This function is Posix only. Too bad for Window$ users
(defun add-trailing-/ (str)
  "Add the trailing slash to the given STR."
  (if (string-match ".+/\$" str)
      str
    (concat str "/")))


;; TODO: Merge this into a unified utils module with the one
;; from `fg42/utils' package
(defun path-join (&rest paths)
  "Join the given PATHS."
  (apply #'concat
         (append
          (mapcar #'add-trailing-/ (butlast paths))
          (last paths))))


(defmacro ->fn (fn-name &rest string-args)
  "Resolve the given FN-NAME to a function by formatting it as a string.
STRING-ARGS are arguments that is going to be passed to `format' function.
For example:
\(->fn \"enma/%-fn\" \"some-name\"\)."
  `(symbol-function
    (intern (format ,fn-name ,@string-args))))


(defmacro -> (x &optional form &rest more)
  "Thread the expr through the forms FORM and rest of form in MORE.
Insert X as the second item in the first form, making a list of
it if it is not a list already.  If there are more forms, insert
the first form as the second item in second form, etc."
  (declare (debug (form &rest [&or symbolp (sexp &rest form)])))
  (cond
   ((null form) x)
   ((null more) (if (listp form)
                    `(,(car form) ,x ,@(cdr form))
                  (list form x)))
   (:else `(-> (-> ,x ,form) ,@more))))


(defmacro ->> (x &optional form &rest more)
  "Thread the expr through the forms FORM and the rest at MORE.
Insert X as the last item in the first form, making a list of
it if it is not a list already.  If there are more forms, insert
the first form as the
last item in second form, etc."
  (declare (debug ->))
  (cond
   ((null form) x)
   ((null more) (if (listp form)
                    `(,@form ,x)
                  (list form x)))
   (:else `(->> (->> ,x ,form) ,@more))))


(defun read-file (path)
  "Return the content of the file at the given PATH as string."
  (when (file-exists-p path)
    (with-temp-buffer
      (insert-file-contents path)
      (buffer-string))))


(defun file->lisp (path)
  "Read the content of the file at PATH and return the Lisp in it."
  (read (or (read-file path) "()")))


(defun lisp->file (path expr)
  "Write the given EXPR to the given file at PATH."
  (with-temp-file path
    (print expr (current-buffer))))


(defmacro comment (&rest body)
  "Ignore the given BODY."
  (declare (indent defun))
  nil)


(defun enma/const (v)
  "Return a constant function of V."
  (lambda (&rest _body)
    v))


(provide 'enma/utils)
;;; utils.el ends here

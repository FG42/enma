;;; Enma --- A functional build tool and package manager for GNU/Emacs
;;
;; Copyright (c) 2020  Sameer Rahmani <lxsameer@gnu.org>
;;
;; Author: Sameer Rahmani <lxsameer@gnu.org>
;; URL: https://gitlab.com/FG42/enma
;; Version: 0.1.0
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;
;;; Commentary:
;;
;;; Code:
(require 'enma/tasks)
(require 'enma/utils)


(defun enma/-compile-dir (TARGET)
  "Compile all the elisp files in the given TARGET regardless of timestamp."
  (message "Compiling '%s'..." target)
  (add-to-list 'load-path target)

  (message "Load path:\n%s\n" (apply #'concat (mapcar (lambda (x) (format "%s\n" x)) load-path)))
  (byte-recompile-directory target 0 t))


(defun enma/compile (dirs)
  "Compile the given DIRS to bytecode."
  (mapc #'enma/-compile-dir dirs))


(deftask compile (dirs)
  "Compile the given DIRS to bytecode.
compile DIRS

Compiles all the ELisp files in the given DIRS to bytecode.
If you have any dependency that is not in your project or
is not part of your Enma setup you need to add them to the
`load-path'."
  (enma/compile dirs))


(defun enma/-clean-dir (dir)
  "Clean up all the elc files from the given DIR."
  (let ((elcs (elc-files-in dir)))
    (when elcs
      (message
       (shell-command-to-string
        (format "rm -v %s" (apply #'concat
                                  (mapcar (lambda (x) (format " %s" x)) elcs))))))))


(defun enma/clean (dirs)
  "Clean the given DIRS from elc files."
  (mapc #'enma/-clean-dir dirs))


(deftask clean (dirs)
  "Clean up the given DIRS from elisp byte codes.
Usage:
enma clean path/to/dir1 path/to/dir2"
  (enma/clean dirs))



(provide 'enma/build)
;;; build.el ends here

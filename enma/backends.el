;;; Enma --- A functional build tool and package manager for GNU/Emacs
;;
;; Copyright (c) 2020  Sameer Rahmani <lxsameer@gnu.org>
;;
;; Author: Sameer Rahmani <lxsameer@gnu.org>
;; URL: https://gitlab.com/FG42/enma
;; Version: 0.1.0
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;
;;; Commentary:
;;; Code:
(require 'enma/utils)
(require 'enma/packages/core)


(defun enma-backends/fetch (state pkg)
  "Fetch the given package PKG which should have a recipe.
STATE is the global state of enma."
  (let* ((recipe (enma-packages/package-recipe pkg))
         (backend (:fetcher (cdr recipe))))
    (when backend
      (let ((fetch-package (->fn "enma-backends-%s/fetch-package" backend)))
        (if fetch-package
            (funcall fetch-package state pkg)
          nil)))))


(provide 'enma/backends)
;;; backends.el ends here

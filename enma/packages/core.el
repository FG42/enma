;;; Enma --- A functional build tool and package manager for GNU/Emacs
;;
;; Copyright (c) 2020  Sameer Rahmani <lxsameer@gnu.org>
;;
;; Author: Sameer Rahmani <lxsameer@gnu.org>
;; URL: https://gitlab.com/FG42/enma
;; Version: 0.1.0
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;
;;; Commentary:
;;
;;; Code:
(require 'cl-lib)


(defun enma-packages/make (name version &optional recipe)
  "Return a plist representing a package with the given information.

NAME is the name of the package on any repository.
VERSION is the version of the package which can be semver or a git commit
or what ever that pkg backends may support.
RECIPE is optional and if it is nil we're going to fill it later based
on the available sources."
  (list 'package
        :name name
        :version version
        :recipe recipe
        :normalized-name (format "%s__%s" name (or version "unspecified"))))

(defun -when-pkg (pkg f)
  "Call F only if PKG is package typed value and return the returning value."
  (when (equal 'package (car pkg))
     (funcall f (cdr pkg))))


(defun -when-pkg! (pkg f)
  "Call F only if PKG is package typed value and return a new package.

It will automatically tag the returning value of the F as a package type."
  (when (equal 'package (car pkg))
     (list 'package
           (funcall f (cdr pkg)))))



(defun enma-packages/package-name (pkg)
  "Return the name of the given PKG."
  (-when-pkg pkg
             (lambda (package)
               (plist-get package :name))))


(defun enma-packages/package-version (pkg)
  "Return the version of the given PKG."
  (-when-pkg pkg
             (lambda (package)
               (plist-get package :version))))


(defun enma-packages/package-recipe (pkg)
  "Return the recipe of the given PKG."
  (-when-pkg pkg
             (lambda (package)
               (plist-get package :recipe))))


(defun enma-packages/package-recipe! (pkg recipe)
  "Return the recipe of the given PKG to RECIPE."
  (-when-pkg! pkg
              (lambda (package)
                (plist-put package :recipe recipe))))


(defun enma-packages/package-normalized-name (pkg)
  "Return the normalized-name of the given PKG."
  (-when-pkg pkg
             (lambda (package)
               (plist-get package :normalized-name))))


(provide 'enma/packages/core)
;;; core.el ends here

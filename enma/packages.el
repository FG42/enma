;;; Enma --- A functional build tool and package manager for GNU/Emacs
;;
;; Copyright (c) 2020  Sameer Rahmani <lxsameer@gnu.org>
;;
;; Author: Sameer Rahmani <lxsameer@gnu.org>
;; URL: https://gitlab.com/FG42/enma
;; Version: 0.1.0
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;
;;; Commentary:
;;
;;; Code:
(require 'cl-lib)
(require 'seq)
(require 'enma/tasks)
(require 'enma/projects)
(require 'enma/state)
(require 'enma/transactions)
(require 'enma/packages/core)
(require 'enma/recipies)


(defun enma/pool-dir ()
  "Return the path to the pool directory which we store packages."
  (concat
   (enma/project-enma-directory enma/target-project) "/pool"))


(defun enma/create-enma-file (path)
  "Create an `enma.el' file at the given PATH with a minimal configuration."
  (when (not (file-exists-p path))
      (copy-file (concat (file-name-as-directory enma/home)
                         "resources/enmafile.el")
                 path)))


(defun enma/load-enma-file (path)
  "Load the `enma.el' file in the given PATH.
This file should contain all the information necessary to build the project
and manage the dependencies of it."
  (when (not (file-exists-p path))
    (throw 'initializaiton-error
           (format "Can't find file '%s'" path)))
  (enma/project-reset-target!)
  (load path))


(defun enma/load-initial-state (project)
  "Load the initial state of the given PROJECT eaithr by creating or loadnig."
  (let ((state (enma/project-state project)))
    (if state
        state
      (enma-state/create (enma/project-state-file project)))))


(defun enma/initialize-pkg-manager (enma-file-path)
  "Initialize the package manager for the given ENMA-FILE-PATH file."
  (enma/load-enma-file enma-file-path)
  (make-directory (enma/project-enma-directory enma/target-project) t)
  (make-directory (enma/pool-dir) t)
  (-> (enma/load-initial-state enma/target-project)
      (enma-sources/fetch-sources enma/target-project)
      (enma-state/commit!)
      (enma-packages/install-packages
       (enma/project-dependencies enma/target-project))
      (enma-state/commit!)))


(defun enma/init (path)
  "Initialize Enma at the given PATH."
  (if path
      (let* ((path (expand-file-name path))
             (enma-file-path (concat (file-name-as-directory path) "enma.el")))
        (if (not (file-exists-p enma-file-path))
            (progn
              (enma/create-enma-file enma-file-path)
              (enma/initialize-pkg-manager enma-file-path))
          (message "The given path already contains an `enma.el'.")))
    (message "`init' commmand needs a destination.")))


(deftask init (paths)
  "Initalize Enma at the car of given PATHS.

Usage:
enma init path/to/the/project

It will create an `enma.el' file, fetch the package recipies and install
the required packages."
  (enma/init (car paths)))


(deftask install (&rest args)
  "Install the dependencies provided in `enma.el' file.

Usage:
enma install

It create a dependency tree from all the packages provided as the value
`:dependencies' key in `enma.el' file and install them all."
  ;; Since we're running in a Task, the enma file should be already
  ;; loaded
  (-> enma/target-project
      (enma/load-initial-state)
      (enma-packages/install-packages
       (enma/project-dependencies enma/target-project))))


(defun enma-packages/load-recipe-from-file (state pkg)
  "Load the recipe for the given PKG from the STATE."
  (enma-recipe/get state
                   (enma-packages/package-name pkg)))


(defun enma-packages/find-recipe (state pkg)
  "Find the recipe for the given PKG from STATE.
It will skip this process if the pkg has a recipe already."
  (let ((recipe (enma-packages/package-recipe pkg)))
    (if recipe
        pkg
      (enma-packages/package-recipe!
       pkg
       (enma-packages/load-recipe-from-file state pkg)))))


(defun enma-packages/installed? (tx package)
  "Return a bool inidicating whether PACKAGE is installed on TX.
TX is a transaction."
  (let ((installed-pkgs (enma-tx/snapshot tx)))
    (when installed-pkgs
      (let ((installed-pkg (assoc (enma-packages/package-name package)
                                  installed-pkgs)))
        (and installed-pkg
             (equal (enma-packages/package-version package)
                    (enma-packages/package-version installed-pkg)))))))


(defun enma-packages/from-list (pkg)
  "Conver the PKG list to `enma-package' struct."
  (enma-packages/make (car pkg)
                      (cadr pkg)
                      (car  (cddr pkg))))



(defun enma-packages/packages-to-install (state packages)
  "Return a list of PACKAGES that needs to be installed based on STATE.
Basically it will diff the given PACKAGES with the packages installed on the
system as the current transaction on STATE."
  (let ((tx (enma-tx/current-tx state)))
    (seq-filter
     (lambda (pkg) pkg)
     (seq-reduce
      (lambda (pkg-list pkg)
        (let ((package (enma-packages/from-list pkg)))
          (when (not (enma-packages/installed? tx package))
            (cons package pkg-list))))
      packages
      '()))))


(defun enma-packages/normalize-name (package)
  "Return the name of the given PACKAGE in <name>__<version> format."
  (format "%s__%s"
          (enma-packages/package-name package)
          (or (enma-packages/package-version package)
              "unspecified")))


(defun enma-packages/install--package (tx normalized-name package cb)
  "Perform the actual installation process for the given PACKAGE.
NORMALIZED-NAME is the normalized name of the package in the format
of <name>__<version>.
CB is a function which is going to get called after installation
has finished with two parameters the PATH to the installed package.
and the EVENT which can be one of the followings:
`:succeed', `:failed'.
TX is the global transaction carrying the installation operation."
  (enma-backends/fetch)
  (funcall cb :succeed "/asd/sad/asd/asd")
  tx)


(defun enma/install-package (tx package remaining-packages)
  "Install the given PACKAGE and update the TX.
After taking care of the PACKAGE installation it will move to the next package
in the REMAINING-PACKAGES queue.  It doesn't matter whether installation was
successful or not.

If the installation is successful it will extract the dependencies of the
PACKAGE and updates the REMAINING-PACKAGES with the new dependencies."
  (lambda (state)
    (if package
        (let ((pkg-name (enma-packages/package-normalized-name package)))
          (if (not (file-exists-p (concat (enma/pool-dir) pkg-name)))
              (progn
                (message "Installing '%s' package..." (enma-packages/package-name package))
                (enma-packages/install--package
                 tx
                 pkg-name
                 package
                 (lambda (event path)
                   (enma/install-package
                    (enma-tx/update-tx!
                     tx
                     (cond ((equal :succeed event) :installed)
                           (t :install-failed))
                     package)
                    (car remaining-packages)
                    (cdr remaining-packages)))))
            tx))
      tx)))



(cl-defun enma-packages/install-packages (state packages &key (concurrent-jobs 0))
  "Install PACKAGES and update the given STATE according to the resutt."
  (let ((packages (enma-packages/packages-to-install state packages)))
    (if packages
      (let* ((packages (mapc (lambda (p) (enma-packages/find-recipe state p))
                             packages))
             (tx (enma-tx/new state
                              (enma-tx/current-tx state)
                              '((:install packages))))
             (partitioned-packages (if (< concurrent-jobs (length packages))
                                       (seq-partition packages concurrent-jobs)
                                     packages)))
        ;;; For now we're going to skip parallel installation to reduce
        ;;; the complexity. But we need to create a partitioned list
        ;;; of the packages and install each partition in a different thread.
        (enma-tx/commit!
         state
         (funcall (enma/install-package tx
                                        (car packages)
                                        (cdr packages))
                  state)))
      state)))


(provide 'enma/packages)
;;; packages.el ends here

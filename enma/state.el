;;; Enma --- A functional build tool and package manager for GNU/Emacs
;;
;; Copyright (c) 2020  Sameer Rahmani <lxsameer@gnu.org>
;;
;; Author: Sameer Rahmani <lxsameer@gnu.org>
;; URL: https://gitlab.com/FG42/enma
;; Version: 0.1.0
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;
;;; Commentary:
;;
;;; Code:
(require 'enma/utils)


(defun enma-state/create (path)
  "Create a new state monad.
PATH is path to the state file to store the data of the state."
  ;; Why not a cl-struct ?
  ;; Because first, we want it to be human readable
  (if (file-exists-p path)
      (file->lisp path)
    (list '(:next-id . 1)
          '(:current-tx . 0)
          '(:transactions . ())
          '(:sources . ())
          '(:recipe-paths . ())
          '(:version . "0")
          (cons :state-file (cons path '())))))


(defun enma-state/get (state k &optional default)
  "Return the value of the given K from the STATE.
It will return DEFAULT in case of a missing key."
  (or (cdr (assoc k state)) default))


(defun enma-state/cons (state k v)
  "Add the given value V to the list addressed by key K on STATE."
  (cons (cons k (cons v (or (enma-state/get state k) '()))) state))


(defun enma-state/recipe-paths (state)
  "Return the value of the `:recipe-paths' from STATE."
  (enma-state/get state :recipe-paths))


(defun enma-state/->recipe-paths! (state v)
  "Add the given value V to the `:recipe-paths' of STATE."
  (enma-state/cons state :recipe-paths v))


(defun enma-state/next-id (state)
  "Return the next available transaction id on STATE."
  (or (enma-state/get state :next-id) 1))


(defun enma-state/add-transaction! (state tx)
  "Add the given TX to the list of transactions of the given STATE."
  (let ((id (enma-tx/id tx)))
    (cons
     (cons :transactions
           (cons tx (enma-state/get state :transactions)))
     (if (> (1+ id) (enma-state/get state :next-id))
         (cons
          (cons :next-id (1+ id))
          state)
       state))))


(defun enma-state/-write-state (state path)
  "Write the given STATE into the given PATH."
  (with-temp-file path
    (print state (current-buffer))))


(defun enma-state/commit! (state)
  "Commit the given state to its file.
The path of the state file comes from `:state-file' key in the STATE."
  (let ((path (cadr (assq :state-file state))))
    (if path
        (enma-state/-write-state state path)
      (throw 'state-error "The given state does not have `:state-file'."))))


'((enma-state/get (enma-state/create "/tmp/a") :current-tx)
  (enma-state/get
   (enma-state/cons
    (enma-state/cons (enma-state/create "/tmp/a") :transactions '(aasd . asda))
    :transactions "zxczxc")
   :transactions)
  (enma-state/commit! (enma-state/create "/tmp/a.state"))
  (enma-state/next-id
   (enma-state/add-transaction!
    (enma-state/add-transaction! (enma-state/create)
                                 '(:id 3 :asd 43))

    '(:id 4 :zxc 499))))

(provide 'enma/state)
;;; state.el ends here

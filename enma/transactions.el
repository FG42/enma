;;; Enma --- A functional build tool and package manager for GNU/Emacs
;;
;; Copyright (c) 2020  Sameer Rahmani <lxsameer@gnu.org>
;;
;; Author: Sameer Rahmani <lxsameer@gnu.org>
;; URL: https://gitlab.com/FG42/enma
;; Version: 0.1.0
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;
;;; Commentary:
;;
;;; Code:
(require 'enma/state)
(require 'enma/packages/core)


(defun enma-tx/new (state parent &optional ops)
  "Create a new transaction for the given STATE with the PARENT.
The optional OPS is a list of operations that has to happen on this
transaction."
  (list :snapshot '()
        :logs '()
        :ops ops
        :parent parent
        :id (enma-state/next-id state)
        :committed nil))


(defun enma-tx/logs (tx)
  "Return the log of the given TX."
  (plist-get tx :logs))


(defun enma-tx/logs! (tx logs)
  "Set the logs of the given TX to LOGS."
  (plist-put tx :logs logs))


(defun enma-tx/committed (tx)
  "Return the commit status of the given TX."
  (plist-get tx :committed))


(defun enma-tx/committed! (tx v)
  "Set the committed value of the given TX to V."
  (plist-put tx :committed v))


(defun enma-tx/snapshot (tx)
  "Return the snapshot of the given TX."
  (plist-get tx :snapshot))


(defun enma-tx/snapshot! (tx value)
  "Set the snapshot of the given TX to VALUE."
  (plist-put tx :snapshot value))

(defun enma-tx/update-snapshot! (tx k v)
  "Update the snapshot of TX with the given key K and value V."
  (enma-tx/snapshot! tx
                     (cons (cons k v) (enma-tx/snapshot tx))))


(defun enma-tx/parent (tx)
  "Return parent of the given TX."
  (plist-get tx :parent))


(defun enma-tx/ops (tx)
  "Return operations of the given TX."
  (plist-get tx :ops))


(defun enma-tx/ops! (tx ops)
  "Set the operations of the given TX to OPS."
  (plist-put tx :ops ops))


(defun enma-tx/id (tx)
  "Return the id of the given TX."
  (plist-get tx :id))


(defun enma-tx/update--tx! (tx op data)
  "Update the snapshot of TX according to the OP and DATA.
OP can be one of the following:
`:installed' which means the packages in DATA is successfully
installed.
`:removed' that indicates the package in DATA is successfully
removed from the pool.
`:install-failed' which means the package installation fialed.
`:remove-failed' which mean removing the package in DATA failed.

DATA can be different based on the type of OP."
  (cond
   ((eq :installed op)
    (enma-tx/update-snapshot! tx (enma-packages/package-name data) data))
   ((eq :removed op) (warn "not implemented"))
   ((eq :install-faild op) (warn "not implemented"))
   ((eq :remove-failed op) (warn "not implemented"))
   (t (throw 'invalid-operation
             (format "Invalid tx state operation '%s'" op)))))


(defun enma-tx/update-tx! (tx op data)
  "Update the given TX with the given OP and DATA.
It will update the TX's `:logs' and `:snapshot' according to the OP."
  (enma-tx/update--tx!
   (enma-tx/logs! tx
                  (cons (cons op data)
                        (enma-tx/logs tx)))
   op
   data))


(defun enma-tx/current-tx (state)
  "Return the current tx id of the given STATE."
  (let ((tx (enma-state/get state :current-tx))
        (txs (enma-state/get state :transactions)))
    (when (and tx (> tx 0))
      (or (assoc (enma-tx/id tx) txs)
          (throw 'inconsistent-state
                 "The given state is inconsistent. current transaction is missing.")))))


(defun enma-tx/commit! (state tx)
  "Commit and finalize the given TX in STATE."
  (enma-state/add-transaction!
   state
   ;; Mark the tx as committed
   (enma-tx/committed! tx t)))


(provide 'enma/transactions)
;;; transactions.el ends here
